# aws-lambda-fileservice

##NOTICES📌

## BIO 📜
#### Shape of .env file
stage=dev
FILE_SERVICE_NAME=xxx
AWS_S3_BUCKET=xxx.xxx.org

## Installation 🚀
Set env file before running commands

Installation of packages
```bash
yarn
```
To run offline
```bash
yarn start
```
To deploy to you aws account via servless && aws-cli
```bash
yarn deploy
```
## Testing 🔬


## Developing 👩🏻‍💻


### Branching 🌿
* ☢️ Master => Main production branch for store builds
* 👩🏻‍💻Dev => Dev branch for all development to merged to
* 👨🏽‍🚀Build => Builds to appcenter for alpha testing on android
#### Creating your own branching:
* feature/branchName => For new features and additions
* fix/branchName => For fixing bugs 
* update/branchName => For updates to packages
#### Pull Requests
All pull requests should be made to the dev branch. Pull request message needs to refrence ticket numbers if applicable.
Please invite the following reviewers to any pull request:
* Frikan Erwee frikan.erwee@openvantage.co.za
