import { S3 } from 'aws-sdk'
import uniqid from 'uniqid';
import sharp from 'sharp';

const s3 = new S3();

/**
 * Summary: File upload handling and image processing.
 *
 * Description: Uploads provided files to aws s3 and additionally performs
 * resize processing on images.
 *
 * @param {Object} event Lambda function invocation event.
 * 
 * @return {Object}      An object containing the response status and payload.
 */
async function fileservice(event) {
  const request= JSON.parse(event.body)
  const { content, type, folder } = request;
  try {

    // check if a request body is present
    if (!content) {
      return {
        statusCode: 400,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: 'Please send a file to upload',
      };
    }

    // get the original file buffer from the request body
    const originalFileBuffer = Buffer.from(content, 'base64');

    // determine the file mime type and extension
    const originalFileType = type;
    let folderId;
    if (folder) {
      folderId = folder
    } else {
      // generate a unique folder ida
      folderId = uniqid();
    }

    // concat folderId with original file name
    const originalFileName = `${folderId}/original`;

    // upload the file to s3
    const { Location: originalLocation } = await upload(
      originalFileName,
      originalFileBuffer,
      originalFileType
    );

    // initialise the response body with the original file url
    let body = {
      original: originalLocation,
      thumbnail: undefined,
      scaled: undefined,
    };

    // check if file is an image, in order to perform image processing
    if (originalFileType.split("/")[0] === "image") {

      // resize the image to 500 x 500 and get the new file buffer
      const thumbnailFileBuffer = await sharp(originalFileBuffer)
        .resize(500, 500)
        .toBuffer()

      // concat folderId with thumbnail file name and file extension
      const thumbnailFileName = `${folderId}/thumbnail`;

      // upload the file to s3
      const { Location: thumbnailLocation } = await upload(
        thumbnailFileName,
        thumbnailFileBuffer,
        originalFileType
      );

      // add the thumbnail image file url to the response body
      body.thumbnail = thumbnailLocation

      // resize the image to 1280 x ? and get the new file buffer
      const scaledFileBuffer = await sharp(originalFileBuffer)
        .resize(1280)
        .toBuffer()

      // concat folderId with thumbnail file name and file extension
      const scaledFileName = `${folderId}/scaled`;

      // upload the file to s3
      const { Location: scaledLocation } = await upload(
        scaledFileName,
        scaledFileBuffer,
        originalFileType
      );

      // add the scaled image file url to the response body
      body.scaled = scaledLocation

    }

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify(body),
    };

  } catch (error) {
    // catch all errors and return to client with 400 status
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify(error),
    };
  }
};

/**
 * Summary: Uploads a file to aws s3.
 *
 * Description: Uploads a file to aws s3.
 *
 * @param {String} blobName Name of the file to upload.
 * @param {Object} buffer   Actual file buffer.
 * @param {Object} type     File mime type.
 * 
 * @return {Object}         An object containing the upload request response.
 */
async function upload(blobName, buffer, type) {
  var params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: blobName,
    Body: buffer,
    ContentType: type,
    ACL: 'public-read'
  };

  return s3.upload(params).promise();
}

exports.fileservice = fileservice